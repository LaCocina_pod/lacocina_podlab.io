---
layout: page
title: Acerca de
permalink: /about/
---

Añade toda la info que quieras compartir

Recuerda que puedes **contactar** con nosotros de las siguientes formas:  
(ejemplo)

+ Twitter: <https://twitter.com/lacocina_pod>
+ Web: <https://lacocina_pod.gitlab.io/>
+ Telegram: <https://t.me/lacocina_pod>
+ Feed Podcast: <https://lacocina_pod.gitlab.io/feed>
